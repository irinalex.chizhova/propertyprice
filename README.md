# propertyprice

Оценка стоимости квартир в Москве на основе данных с общедоступных ресурсов.

## Getting started

Скачивание репозитория и установка зависимостей:

```bash
git clone https://gitlab.com/irinalex.chizhova/propertyprice.git

python -m venv venv
venv/bin/activate

pip3 install --upgrade pip
pip install -r requirements.txt
```

Далее необходимо подготовить файл данных с циана:

```bash
git clone https://gitlab.com/irinalex.chizhova/propertyprice.git

python -m venv venv
venv/bin/activate

pip3 install --upgrade pip
pip install -r requirements.txt
```

Добавляем файл в data/raw/cian/<название>.csv
Далее в файле params/process_data.yaml исправляем строчку:

```yaml
in_data_cian:
  data/raw/cian/<название>.csv

```

Запускаем процесс обработки данных:

```bash
python src/process_data.py
```

Обучение модели:

```bash
python src/train.py
```

Далее делаем проверку точности модели:

```bash
python src/evaluate.py
```

## Инструкции

Инструкция по установке [docker](https://docs.docker.com/engine/install/ubuntu/)

Инструкция по установке [gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html)
